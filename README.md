# OpenML dataset: Best-Books-of-the-19th-Century

https://www.openml.org/d/43791

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset has been scrapped off Goodreads to obtain land information about the best books of the 19th Century.  



Feature
Description




Book_Name
the title of the book


Author_Name
the author(s) of the book


Description
a brief description about the book


Rating
rating given by Goodreads users


CoverImageLink
the book cover

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43791) of an [OpenML dataset](https://www.openml.org/d/43791). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43791/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43791/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43791/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

